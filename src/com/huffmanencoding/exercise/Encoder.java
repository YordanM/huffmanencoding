package com.huffmanencoding.exercise;

import java.util.*;

class Encoder {
	private PriorityQueue<Node> queue;
	private String message;
	private Map<Character, String> codeMap = new HashMap<>();
	private Map<String, Character> decodeMap = new HashMap<>();

	// Constructor, which builds the binary tree from a single String message using
	// a PriorityQueue collection
	Encoder(String message) {
		this.message = message;

		HashMap<Character, Integer> weightMap = new HashMap<>();
		for (char symbol : message.toCharArray())
			weightMap.put(symbol, (weightMap.get(symbol) == null) ? 1 : weightMap.get(symbol) + 1);
		queue = new PriorityQueue<>(Comparator.comparing(Node::getWeight));
		for (Character symbol : weightMap.keySet()) {
			queue.add(new Node(symbol, weightMap.get(symbol)));
		}
		while (queue.size() > 1) {
			Node left = queue.poll();
			Node right = queue.poll();
			if (right != null && left != null) {
				Node currentNode = new Node('\u0000', left.getWeight() + right.getWeight(), left, right);
				queue.offer(currentNode);
			}
		}
		fillMaps(queue.peek(), "");
	}

	private void fillMaps(Node root, String codeValue) {
		if (root == null)
			return;
		if (root.getLeft() == null && root.getRight() == null) {
			codeMap.put(root.getSymbol(), codeValue);
			decodeMap.put(codeValue, root.getSymbol());
		}
		fillMaps(root.getLeft(), codeValue + "0");
		fillMaps(root.getRight(), codeValue + "1");
	}

	String getEncodedMessage() {
		StringBuilder encodedMessage = new StringBuilder();
		for (char symbol : message.toCharArray())
			encodedMessage.append(codeMap.get(symbol));
		return encodedMessage.toString();
	}

	// Returns a decoded message passed as a parameter
	String getDecodedMessage(String code) {
		StringBuilder decodedMessage = new StringBuilder();
		StringBuilder bit = new StringBuilder();
		for (char symbol : code.toCharArray()) {
			bit.append(symbol);
			if (decodeMap.containsKey(bit.toString())) {
				decodedMessage.append(decodeMap.get(bit.toString()));
				bit.setLength(0);
			}
		}
		return decodedMessage.toString();
	}

	// Calculates the height of a binary tree
	private static int height(Node root) {
		if (root == null)
			return -1;
		return Math.max(height(root.getLeft()), height(root.getRight())) + 1;
	}

	// Returns the height of the current instance's binary tree
	int getHeight() {
		return height(this.queue.peek());
	}
}
