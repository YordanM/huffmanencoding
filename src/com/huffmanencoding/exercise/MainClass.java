package com.huffmanencoding.exercise;

public class MainClass {
	public static void main(String[] args) {
		String message = "Lorem ipsum dolor sit amet, consectetur adipisicing elit,"
				+ " sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
				+ " Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris"
				+ " nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in"
				+ " reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla"
				+ " pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa"
				+ " qui officia deserunt mollit anim id est laborum.";

		Encoder encoder = new Encoder(message);

		// Encoding the message and printing it's length
		System.out.println("Length of code: " + encoder.getEncodedMessage().length());

		// Printing the binary tree's height
		System.out.println("Height of binary tree: " + encoder.getHeight());
	}
}
