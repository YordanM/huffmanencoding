package com.huffmanencoding.exercise;

class Node {
	private char symbol;
	private int weight;
	private Node left = null;
	private Node right = null;

	Node(char symbol, int weight) {
		this.symbol = symbol;
		this.weight = weight;
	}

	Node(char symbol, int weight, Node left, Node right) {
		this.symbol = symbol;
		this.weight = weight;
		this.left = left;
		this.right = right;
	}

	char getSymbol() {
		return symbol;
	}

	int getWeight() {
		return weight;
	}

	Node getLeft() {
		return this.left;
	}

	Node getRight() {
		return this.right;
	}
}
